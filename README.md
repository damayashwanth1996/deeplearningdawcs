# DeepLearningDAWCS

An attention-based model for predictive analytics on Wireless Communication System. The transformers model and traditional Autoencoder integration leverage the relationships between 3 datasets simultaneously.
