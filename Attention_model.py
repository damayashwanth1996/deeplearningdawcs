import tensorflow as tf
import numpy as np
import pandas as pd
import logging
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.callbacks import EarlyStopping
import matplotlib.pyplot as plt
import os

print("Attention Model")

logging.getLogger('tensorflow').setLevel(logging.ERROR)  # suppress warnings

gpus = tf.config.experimental.list_physical_devices('GPU')
print(gpus)
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)


def creat_data(dataset, num_steps):
    X, Y = [], []
    for i in range(len(dataset) - num_steps - 1):
        a = dataset[i:(i + num_steps), :]
        b = dataset[i + 1:(i + num_steps) + 1, :]
        X.append(a)
        Y.append(b)
    return np.array(X), np.array(Y)


class My_MSE(tf.keras.losses.Loss):
    def __init__(self, name="My_MSE"):
        super().__init__(name=name)

    def call(self, y_true, y_pred):
        print("Hello")
        print(y_pred.shape)
        outputlink1 = y_pred[:, 0]
        outputlink2 = y_pred[:, 1]
        outputlink3 = y_pred[:, 2]
        # output2 = y_pred[:, :, 15:]
        print('loss')
        print(outputlink1.shape)
        print(y_true.shape)
        print(y_true[:, 1].shape)
        loss_sub1 = tf.losses.mean_squared_error(outputlink1, y_true[:, 0])
        loss_sub2 = tf.losses.mean_squared_error(outputlink2, y_true[:, 1])
        loss_sub3 = tf.losses.mean_squared_error(outputlink3, y_true[:, 2])
        # loss_tar = tf.losses.mean_squared_error(output2, tf.concat([y_true[:, 0], y_true[:, 1], y_true[:, 2]], axis=-1))
        loss_sub = loss_sub1 + loss_sub2 + loss_sub3
        loss = loss_sub  # + loss_tar
        return loss


class My_MSE_sub(tf.keras.losses.Loss):
    def __init__(self, name="My_MSE"):
        super().__init__(name=name)

    def call(self, y_true, y_pred):
        # tf.print(y_pred.shape)
        loss = tf.losses.mean_squared_error(y_pred, y_true)
        return loss


# defining self attention
def dot_product_attention(q, k, v):
    # calculate attention weights from query, key and value
    # the dimensions of q, k, and v are equal i.e sequence_length of q = seq_length of k = seq_length of v

    matmul_qk = tf.matmul(q, k, transpose_b=True)
    dk = tf.cast(tf.shape(k)[-1], tf.float32)

    scaled_attention = matmul_qk / tf.math.sqrt(dk)
    # for scaling we perform a softmax operation of dot product of matmul_qk and dk
    # performing softmax will result out in the sum of scores being 1
    attention_weights = tf.nn.softmax(scaled_attention, axis=-1)

    output = tf.matmul(attention_weights, v)

    return output, attention_weights


# defining multi head attention
class MultiHeadAttention(tf.keras.layers.Layer):
    def __init__(self, d_units, num_heads):
        super(MultiHeadAttention, self).__init__()
        self.num_heads = num_heads
        self.d_units = d_units

        assert d_units % num_heads == 0

        self.depth = self.d_units // self.num_heads

        self.wq = tf.keras.layers.Dense(d_units)
        self.wk = tf.keras.layers.Dense(d_units)
        self.wv = tf.keras.layers.Dense(d_units)

        self.dense = tf.keras.layers.Dense(d_units)

    def split_heads(self, x, batch_size):
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.depth))

        return tf.transpose(x, perm=[0, 2, 1, 3])  # shape = (batch size, num_heads, time steps, depth)

    def call(self, q, k, v):
        batch_size = tf.shape(q)[0]

        q = self.wq(q)  # (batch_size, seq_len, d_units)
        k = self.wk(k)  # (batch_size, seq_len, d_units)
        v = self.wk(v)  # (batch_size, seq_len, d_units)

        q = self.split_heads(q, batch_size)  # (batch_size, num_heads, time steps, depth)
        k = self.split_heads(k, batch_size)  # (batch_size, num_heads, time steps, depth)
        v = self.split_heads(v, batch_size)  # (batch_size, num_heads, time steps, depth)

        scaled_attention, attention_weights = dot_product_attention(
            q, k, v)  # scaled_attention shape = (batch size, num_heads, time steps, depth)
        # attention_weight shape = (batch size, num_heads, time steps q, time steps k) because of multiplication of q and k

        scaled_attention = tf.transpose(scaled_attention, perm=[0, 2, 1, 3])
        # after transposing scaled_attention shape = (batch size, time steps, num_heads, depth)

        concat_attention = tf.reshape(scaled_attention, (batch_size, -1, self.d_units))

        output = self.dense(concat_attention)

        return output, attention_weights


def feed_forward_network(d_units, dff):
    return tf.keras.Sequential([
        tf.keras.layers.Dense(dff, activation='relu'),  # (batch size, time steps, dff)
        tf.keras.layers.Dense(d_units)
    ])


class LSTMFramework(tf.keras.Model):
    def __init__(self, time_steps, d_units, num_heads, dff, units, hidden_units, rate):
        super().__init__()
        self.time_steps = time_steps
        self.d_units = d_units
        self.num_heads = num_heads
        self.dff = dff
        self.rate = rate
        self.hidden_units = hidden_units

        self.units = units
        self.submodel1 = SubModel(time_steps, units, hidden_units)
        self.submodel2 = SubModel(time_steps, units, hidden_units)
        self.submodel3 = SubModel(time_steps, units, hidden_units)

        self.attention1 = Attention(d_units, num_heads, dff, units, hidden_units, rate)
        self.attention2 = Attention(d_units, num_heads, dff, units, hidden_units, rate)
        self.attention3 = Attention(d_units, num_heads, dff, units, hidden_units, rate)

    def build(self, input_shape):
        self.tarmodel1 = TarModel(self.time_steps, self.units, input_shape[-1])
        self.tarmodel2 = TarModel(self.time_steps, self.units, input_shape[-1])
        self.tarmodel3 = TarModel(self.time_steps, self.units, input_shape[-1])
        self.built = True

    def train_step(self, data):
        # Unpack the data. Its structure depends on your model and
        # on what you pass to `fit()`.
        x, y = data

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True)  # Forward pass
            print('err')
            print(y.shape)
            print(y_pred.shape)
            loss = self.compiled_loss(y, y_pred, regularization_losses=self.losses)

        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        # Update metrics (includes the metric that tracks the loss)
        self.compiled_metrics.update_state(y, y_pred)
        # Return a dict mapping metric names to current value
        return {m.name: m.result() for m in self.metrics}

    @tf.function
    def call(self, inputs):
        input_link1 = inputs[:, 0, :, :]
        input_link2 = inputs[:, 1, :, :]
        input_link3 = inputs[:, 2, :, :]
        outputlink1, olink1 = self.submodel1(input_link1)
        outputlink2, olink2 = self.submodel2(input_link2)
        outputlink3, olink3 = self.submodel3(input_link3)
        outlink1, attn_out1 = self.attention1(inputs=tf.concat([olink1, olink2, olink3], axis=-1))
        outlink2, attn_out2 = self.attention2(inputs=tf.concat([olink1, olink2, olink3], axis=-1))
        outlink3, attn_ou3 = self.attention3(inputs=tf.concat([olink1, olink2, olink3], axis=-1))
        # state = [tf.zeros(shape=(inputs.shape[0], self.units)), tf.zeros(shape=(inputs.shape[0], self.units))]
        # state = [state1[0] + state2[0] + state3[0], state1[1] + state2[1] + state3[1]]
        output2 = self.tarmodel1(inputs=tf.math.multiply(olink1, outlink1))
        output3 = self.tarmodel2(inputs=tf.math.multiply(olink2, outlink2))
        output4 = self.tarmodel3(inputs=tf.math.multiply(olink3, outlink3))
        # return tf.concat([output2, output3, output4], axis=-1)
        # print('shape')
        # print(output2.shape)
        output2 = tf.reshape(output2, [-1, 1, output2.shape[1], output2.shape[2]])
        output3 = tf.reshape(output3, [-1, 1, output3.shape[1], output3.shape[2]])
        output4 = tf.reshape(output4, [-1, 1, output4.shape[1], output4.shape[2]])
        # print(output2.shape, output3.shape, output4.shape)
        return tf.concat([output2, output3, output4], axis=1)


class TarModel(tf.keras.Model):
    def __init__(self, time_steps, units, output_num):
        super().__init__()
        self.time_steps = time_steps
        self.units = units
        self.output_num = output_num

        self.lstm = tf.keras.layers.LSTM(units=self.units,
                                         return_sequences=True, recurrent_activation=tf.nn.tanh,
                                         kernel_initializer=tf.keras.initializers.RandomUniform)

        self.dense3 = tf.keras.layers.Dense(output_num, activation=tf.nn.relu,
                                            kernel_initializer=tf.keras.initializers.RandomUniform)

    @tf.function
    def call(self, inputs):
        output = self.dense3(self.lstm(inputs=inputs))
        return output


class SubModel(tf.keras.Model):
    def __init__(self, time_steps, units, hidden_units):
        super().__init__()
        self.time_steps = time_steps
        self.hidden_units = hidden_units
        self.units = units
        self.lstm1 = tf.keras.layers.LSTM(units=self.units, return_sequences=True, activation=tf.nn.tanh,
                                          kernel_initializer=tf.keras.initializers.RandomUniform)
        self.lstm2 = tf.keras.layers.LSTM(units=self.units, return_sequences=True, activation=tf.nn.tanh,
                                          kernel_initializer=tf.keras.initializers.RandomUniform)
        self.dense = tf.keras.layers.Dense(self.hidden_units, activation=tf.nn.relu,
                                           kernel_initializer=tf.keras.initializers.RandomUniform)

    def build(self, input_shape):
        self.dense2 = tf.keras.layers.Dense(input_shape[-1], activation=tf.nn.relu,
                                            kernel_initializer=tf.keras.initializers.RandomUniform)
        self.dense3 = tf.keras.layers.Dense(input_shape[-1], activation=tf.nn.relu,
                                            kernel_initializer=tf.keras.initializers.RandomUniform)
        self.built = True

    @tf.function
    def call(self, inputs):
        lstm1_o = self.dense2(self.lstm1(inputs))
        output1 = self.dense(lstm1_o)
        lstm2_i = tf.concat([output1, lstm1_o], axis=-1)
        output2 = self.dense3(self.lstm2(lstm2_i))
        return output2, output1
        #   output 1 shape = (none, 12, 3)
        # output 2 shape = (none, 12, 5)


class Attention(tf.keras.Model):
    def __init__(self, d_units, num_heads, dff, units, hidden_units, rate):
        super(Attention, self).__init__()
        self.units = units
        self.hidden_units = hidden_units
        self.rate = rate

        self.mha = MultiHeadAttention(d_units, num_heads)
        self.ffn = feed_forward_network(d_units, dff)

        self.layernorm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(rate)
        self.dropout2 = tf.keras.layers.Dropout(rate)

        self.dense = tf.keras.layers.Dense(units=d_units)
        self.lstm = tf.keras.layers.LSTM(units=self.units, return_sequences=True, activation=tf.nn.tanh,
                                         kernel_initializer=tf.keras.initializers.RandomUniform)
        self.dense1 = tf.keras.layers.Dense(self.hidden_units, activation=tf.nn.relu,
                                            kernel_initializer=tf.keras.initializers.RandomUniform)

    def build(self, input_shape):
        # print(input_shape)
        self.dense2 = tf.keras.layers.Dense(input_shape[-1], activation=tf.nn.relu,
                                            kernel_initializer=tf.keras.initializers.RandomUniform)
        self.built = True

    def call(self, inputs):
        transform_inputs = self.dense(inputs)  # making the inputs shape equal to attention output shape for addition
        attn_output, _ = self.mha(inputs, inputs, inputs)  # (batch size, time steps, d_units)
        attn_output = self.dropout1(attn_output)

        out1 = self.layernorm1(transform_inputs + attn_output)  # (batch size, time steps, d_units)

        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output)
        out2 = self.layernorm2(out1 + ffn_output)


        # out3 = self.dense2(self.lstm(out2))
        #out3 = self.lstm(out2)
        output = self.dense1(out2)


        return output, out2


# class AttentionModel(tf.keras.Model):
#     def __init__(self, time_steps, units, hidden_units):
#         super().__init__()
#         self.time_steps = time_steps
#         self.units = units
#         self.hidden_units = hidden_units
#         self.lstm = tf.keras.layers.LSTM(units=self.units, return_sequences=True, activation=tf.nn.tanh,
#                                          kernel_initializer=tf.keras.initializers.RandomUniform)
#         self.lstm1 = tf.keras.layers.LSTM(self.hidden_units, return_sequences=True, activation=tf.nn.tanh,
#                                           kernel_initializer=tf.keras.initializers.RandomUniform)
#         #self.dense4 = tf.keras.layers.Dense(self.hidden_units, activation=tf.nn.relu,
#         #                                    kernel_initializer=tf.keras.initializers.RandomUniform)
#
#     def build(self, input_shape):
#         self.dense = tf.keras.layers.Dense(input_shape[-1], activation=tf.nn.relu,
#                                            kernel_initializer=tf.keras.initializers.RandomUniform)
#         self.dense4 = tf.keras.layers.Dense(input_shape[-1], activation=tf.nn.relu,
#                                             kernel_initializer=tf.keras.initializers.RandomUniform)
#         self.built = True
#
#     @tf.function
#     def call(self, inputs, training=None, mask=None):
#         lstm_out = self.lstm(self.dense(inputs))
#         output3 = self.lstm1(self.dense4(lstm_out))
#         return output3

columns = ['TT', 'UT', 'LMS', 'CML', 'MLR']

time_steps = 12
x_train_list = []
x_test_list = []
y_train_list = []
y_test_list = []
for p in ['LL1', 'LL2downlink', 'LL3downlink']:
    path = r'C:\Users\damay\OneDrive\Desktop\Thesis\data\100m/' + p + '.csv'
    datalink = pd.read_csv(path, usecols=columns).values
    datalink = MinMaxScaler().fit_transform(datalink)
    X, Y = creat_data(datalink, time_steps)
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.3)
    x_train_list.append(x_train)
    x_test_list.append(x_test)
    y_train_list.append(y_train)
    y_test_list.append(y_test)

model = LSTMFramework(time_steps=time_steps, units=64, d_units=32, num_heads=1, dff=128,
                      hidden_units=3, rate=0.1)
optimizer = tf.keras.optimizers.Adam()
batch_size = 200
ep = 100
# callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=5, mode='min')
# submodel1 = model.submodel1
# submodel2 = model.submodel2
# submodel3 = model.submodel3
# submodel1.compile(optimizer='Adam', loss={'output_1': My_MSE_sub(), 'output_2': None})
# history1 = submodel1.fit(x=x_train_list[0], y=y_train_list[0], epochs=ep, batch_size=batch_size, validation_split=0.3, verbose=1)
# submodel2.compile(optimizer='Adam', loss={'output_1': My_MSE_sub(), 'output_2': None})
# history2 = submodel2.fit(x=x_train_list[1], y=y_train_list[1], epochs=ep, batch_size=batch_size, validation_split=0.3, verbose=1)
# submodel3.compile(optimizer='Adam', loss={'output_1': My_MSE_sub(), 'output_2': None})
# history3 = submodel3.fit(x=x_train_list[2], y=y_train_list[2], epochs=ep, batch_size=batch_size, validation_split=0.3, verbose=1)
# outputlink1, olink1 = submodel1(x_train_list[0])
# outputlink2, olink2 = submodel2(x_train_list[1])
# outputlink3, olink3 = submodel3(x_train_list[2])
# loss_sub1 = tf.losses.mean_squared_error(outputlink1, y_train_list[0])
# loss_sub2 = tf.losses.mean_squared_error(outputlink2, y_train_list[1])
# loss_sub3 = tf.losses.mean_squared_error(outputlink3, y_train_list[2])
# single_loss_sub = loss_sub1 + loss_sub2 + loss_sub3

x = np.swapaxes(np.array(x_train_list), 0, 1)
y = np.swapaxes(np.array(y_train_list), 0, 1)
x_test = np.swapaxes(np.array(x_test_list), 0, 1)
y_test = np.swapaxes(np.array(y_test_list), 0, 1)
model.compile(optimizer='adam', loss={'output_1': My_MSE_sub()})
history = model.fit(x=x, y=y, epochs=ep, batch_size=batch_size, validation_split=0.3,
                    verbose=1)  # callbacks=[callback],
y_pred_train = model(x)

train_mae_loss = np.mean(np.abs(y_pred_train, y))
train_mae_loss = train_mae_loss.reshape((-1))
threshold = np.max(train_mae_loss)
print("Reconstruction error threshold: ", threshold)

loss_sub1 = tf.losses.mean_squared_error(y_pred_train[:, 0, :, :], y[:, 0, :, :])
loss_sub2 = tf.losses.mean_squared_error(y_pred_train[:, 1, :, :], y[:, 1, :, :])
loss_sub3 = tf.losses.mean_squared_error(y_pred_train[:, 2, :, :], y[:, 2, :, :])
loss_sub = loss_sub1 + loss_sub2 + loss_sub3
# outputlink1 = y_pred_train[:, 0, :, :]
# outputlink2 = y_pred_train[:, 1, :, :]
# outputlink3 = y_pred_train[:, 2, :, :]
# #output2 = y_pred_train[:, :, 15:]
# loss_sub1 = tf.losses.mean_squared_error(outputlink1, y[:, 0])
# loss_sub2 = tf.losses.mean_squared_error(outputlink2, y[:, 1])
# loss_sub3 = tf.losses.mean_squared_error(outputlink3, y[:, 2])
# #loss_tar = tf.losses.mean_squared_error(output2, tf.concat([y[:, 0], y[:, 1], y[:, 2]], axis=-1))
# loss_sub = loss_sub1 + loss_sub2 + loss_sub3

print("loss_sub_train", np.mean(loss_sub))  # "loss_tar_train:", np.mean(loss_tar))

y_pred_test = model(x_test)
test_mae_loss = np.mean(np.abs(y_pred_test - y_test))
test_mae_loss = test_mae_loss.reshape((-1))

test_loss1 = tf.losses.mean_squared_error(y_pred_test[:, 0, :, :], y_test[:, 0, :, :])
test_loss2 = tf.losses.mean_squared_error(y_pred_test[:, 1, :, :], y_test[:, 1, :, :])
test_loss3 = tf.losses.mean_squared_error(y_pred_test[:, 2, :, :], y_test[:, 2, :, :])
test_loss = test_loss1 + test_loss2 + test_loss3

print("Test mae loss", np.max(test_mae_loss))
# print("Train accuracy", np.mean(history.history['accuracy']))
# print("Validation accuracy", np.mean(history.history['val_accuracy']))

train_loss_multiple = history.history['loss']
val_loss_multiple = history.history['val_loss']

# link1_train_loss = history1.history['loss']
# link1_val_loss = history1.history['val_loss']
#
# link2_train_loss = history2.history['loss']
# link2_val_loss = history2.history['val_loss']
#
# link3_train_loss = history3.history['loss']
# link3_val_loss = history3.history['val_loss']

outputlink1 = y_pred_test[:, :, 0:5]
outputlink2 = y_pred_test[:, :, 5:10]
outputlink3 = y_pred_test[:, :, 10:15]
# output2 = y_pred_test[:, :, 15:]
# loss_sub1 = tf.losses.mean_squared_error(outputlink1, y_test[:, 0])
# loss_sub2 = tf.losses.mean_squared_error(outputlink2, y_test[:, 1])
# loss_sub3 = tf.losses.mean_squared_error(outputlink3, y_test[:, 2])
# loss_tar = tf.losses.mean_squared_error(output2, tf.concat([y_test[:, 0], y_test[:, 1], y_test[:, 2]], axis=-1))
# loss_sub = loss_sub1 + loss_sub2 + loss_sub3

print("loss_sub_test", np.mean(test_loss))  # "loss_tar_valid_test:", np.mean(loss_tar))
# print("single_training_losses", np.mean(single_loss_sub))

os.makedirs("Single_attention_losses", exist_ok=True)
pd.DataFrame(list(zip(train_loss_multiple, val_loss_multiple)),
             columns=['model_train_loss', 'model_val_loss']).to_csv(
    "Single_attention_losses/variant2loss.csv",
    index=None)

#pd.DataFrame(
  #  list(zip(link1_train_loss, link1_val_loss, link2_train_loss, link2_val_loss, link3_train_loss, link3_val_loss)),
 #   columns=['dataset1_train_loss', 'dataset1_val_loss', 'dataset2_train_loss', 'dataset2_val_loss',
  #           'dataset3_train_loss', 'dataset3_val_loss']).to_csv("Single_attention_losses/variantsinglelossvsepoch.csv",
    #                                                             index=None)

# plt.figure(figsize=(12, 6))
# plt.plot(history.history['loss'], '-r', label='Training Loss')
# plt.plot(history.history['val_loss'], '-b', label='Validation Loss')
# plt.title('Training and Validation Loss', fontsize=15)
# plt.ylabel('Loss', fontsize=15)
# plt.xlabel('Epoch #', fontsize=15)
# plt.legend(loc='upper right', fontsize='large')
#
# plt.figure(figsize=(12, 6))
# plt.plot(history.history['accuracy'], '-r', label="Training Accuracy")
# plt.plot(history.history['val_accuracy'], '-b', label="Validation Accuracy")
# plt.title('Training and Validation Accuracy', fontsize=15)
# plt.xlabel('Epoch #', fontsize=15)
# plt.ylabel('Accuracy', fontsize=15)
# plt.legend(loc='upper right', fontsize='large')
#
# plt.figure(figsize=(12, 6))
# plt.plot(y_test[:, 0, -1, 0], label="Ground truth")
# plt.plot(outputlink1[:, -1, 0], label="Predictions")
# plt.title('Actual and Predicted Values for Dataset 1', fontsize=15)
# plt.xlabel('Number of Samples', fontsize=15)
# plt.ylabel('Feature Values', fontsize=15)
# plt.legend(loc='upper right', fontsize='large')
#
# plt.figure(figsize=(12, 6))
# plt.plot(y_test[:, 1, -1, 0], label="Ground truth")
# plt.plot(outputlink1[:, -1, 0], label="Predictions")
# plt.title('Actual and Predicted Values for Dataset 2', fontsize=15)
# plt.xlabel('Number of Samples', fontsize=15)
# plt.ylabel('Feature Values', fontsize=15)
# plt.legend(loc='upper right', fontsize='large')
#
# plt.figure(figsize=(12, 6))
# plt.plot(y_test[:, 2, -1, 0], label="Ground truth")
# plt.plot(outputlink1[:, -1, 0], label="Predictions")
# plt.title('Actual and Predicted Values for Dataset 3', fontsize=15)
# plt.xlabel('Number of Samples', fontsize=15)
# plt.ylabel('Feature Values', fontsize=15)
# plt.legend(loc='upper right', fontsize='large')
#
# plt.show()
# fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(5, figsize = (10, 8), sharex=True, sharey=True)
# ax1.plot(x_test[0:100, 2, -1, 0])
# ax1.plot(outputlink3[0:100, -1, 0])
# ax2.set_title("TT")
#
# ax2.plot(x_test[0:100, 2, -1, 1])
# ax2.plot(outputlink3[0:100, -1, 1])
# ax2.set_title("UT")
#
# ax3.plot(x_test[0:100, 2, -1, 2])
# ax3.plot(outputlink3[0:100, -1, 2])
# ax2.set_title("LMS")
#
# ax4.plot(x_test[0:100, 2, -1, 3])
# ax4.plot(outputlink3[0:100, -1, 3])
# ax2.set_title("CML")
#
# ax5.plot(x_test[0:100, 2, -1, -1])
# ax5.plot(outputlink3[0:100, -1, -1])
# ax5.set_title("MLR")
#
# plt.xlabel('Test_Size', fontsize='large')
# plt.ylabel('Loss', fontsize='large')
# plt.legend(['True', 'Pred'], loc='upper right')
# plt.show()
